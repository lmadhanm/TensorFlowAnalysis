import sys, os, math
sys.path.append("../")
os.environ["CUDA_VISIBLE_DEVICES"] = ""

from TensorFlowAnalysis import *

if __name__ == "__main__" : 

  # Masses of the initial and final state particles
  mb    = 5.279
  mdst  = 2.010
  mdsst = 2.112
  mds   = 1.940
  mdz   = 1.865
  mpi   = 0.139

  # Radial constants for Blatt-Weisskopf form factors
  db = Const(5.)
  dr = Const(1.5)

  integ_points = 100

  # Set random seed
  SetSeed(1)

  # Imaginary "i"
  I = Complex(Const(0.), Const(1.))

  # Generation phase space is the direct product of Dalitz and 4D angular phase space in ( cos(theta_d),  phi_d, cos(theta_ds), phi_ds )
  dalitzPhsp = DalitzPhaseSpace(mdst, mpi, mdsst, mb)
  angularPhsp = RectangularPhaseSpace( ((-1., 1.), (-math.pi, math.pi), (-1., 1.), (-math.pi, math.pi)) )
  phsp = CombinedPhaseSpace(dalitzPhsp, angularPhsp)

  # Couplings for "10" and "00" helicities
  c11 = Complex( Const(1.), Const(0.) )
  c10 = Complex( FitParameter("c10r", -1., -10., 10.), FitParameter("c10i", 1., -10., 10.) )
  c01 = Complex( FitParameter("c01r", -1., -10., 10.), FitParameter("c01i", 1., -10., 10.) )
  c00 = Complex( FitParameter("c00r", -1., -10., 10.), FitParameter("c00i", 1., -10., 10.) )

  d11 = Complex( FitParameter("d11r", -1., -10., 10.), FitParameter("d11i", 1., -10., 10.) )
  d10 = Complex( FitParameter("d10r", -1., -10., 10.), FitParameter("d10i", 1., -10., 10.) )
  d01 = Complex( FitParameter("d01r", -1., -10., 10.), FitParameter("d01i", 1., -10., 10.) )
  d00 = Complex( FitParameter("d00r", -1., -10., 10.), FitParameter("d00i", 1., -10., 10.) )

  # Dictrionary of couplings,
  c = { 
    (-2, -2, -2) :  c11, 
    (-2, -2,  0) :  c10, 
    (-2, -2,  2) :  c11, 
    (-2,  2, -2) : -c11, 
    (-2,  2,  0) : -c10, 
    (-2,  2,  2) : -c11, 

    ( 2, -2, -2) :  c11, 
    ( 2, -2,  0) :  c10, 
    ( 2, -2,  2) :  c11, 
    ( 2,  2, -2) : -c11, 
    ( 2,  2,  0) : -c10, 
    ( 2,  2,  2) : -c11, 

    ( 0, -2, -2) :  c01, 
    ( 0, -2,  0) :  c00, 
    ( 0, -2,  2) :  c01, 
    ( 0,  2, -2) : -c01, 
    ( 0,  2,  0) : -c00, 
    ( 0,  2,  2) : -c01, 
  }

  d = { 
    (-2, -2, -2) :  d11, 
    (-2, -2,  0) :  d10, 
    (-2, -2,  2) : -d11, 
    (-2,  2, -2) : -d11, 
    (-2,  2,  0) : -d10, 
    (-2,  2,  2) :  d11, 

    ( 2, -2, -2) :  d11, 
    ( 2, -2,  0) :  d10, 
    ( 2, -2,  2) : -d11, 
    ( 2,  2, -2) : -d11, 
    ( 2,  2,  0) : -d10, 
    ( 2,  2,  2) :  d11, 

    ( 0, -2, -2) :  d01, 
    ( 0, -2,  0) :  d00, 
    ( 0, -2,  2) : -d01, 
    ( 0,  2, -2) : -d01, 
    ( 0,  2,  0) : -d00, 
    ( 0,  2,  2) :  d01, 
  }

  def model(x) : 
    """
      Description of the decay density as a function of the vector of input 
      parameters x.
    """
    dlz = phsp.Data1(x)  # Dalitz plot variables
    ang = phsp.Data2(x)  # Angles
    m2dstpi = dalitzPhsp.M2ab(dlz)
    cos_th_dst = dalitzPhsp.CosHelicityAB(dlz)    # D* helicity angle
    cos_th_d   = angularPhsp.Coordinate(ang, 0)   # D helicity angle
    phi_d      = angularPhsp.Coordinate(ang, 1)   # azimuthal angle of D*->Dpi
    cos_th_ds  = angularPhsp.Coordinate(ang, 2)   # Ds helicity angle
    phi_ds     = angularPhsp.Coordinate(ang, 3)   # azimuthal angle of Ds*->Dsgamma

    j_dstst = 2  # D** spin == 1 (spins are always doubled)
    j_dstst2 = 4  # D** spin == 2

    # Construct the amplitude of only one intermediate resonance, D1(2420)->D*pi

    density = Const(0.)
    for lambda_gamma in [-2, 2] : 
      ampl1 = Complex(Const(0.), Const(0.))
      ampl2 = Complex(Const(0.), Const(0.))
      # Loop over D*, Ds* helicities (allowed ones are -1, 0, 1)
      for lambda_dst in [-2, 0, 2] : 
        for lambda_ds in [-2, 0, 2] : 
          # Helicity term in the amplitude (for J^P=1+)
          h1 = CastComplex(Wignerd(Acos(cos_th_dst), j_dstst, lambda_ds, lambda_dst))*\
               Exp(I*CastComplex(lambda_dst/2.*phi_d))*\
               CastComplex(Wignerd(Acos(cos_th_d), 2, lambda_dst, 0))*\
               Exp(I*CastComplex(lambda_ds/2.*phi_ds))*\
               CastComplex(Wignerd(Acos(cos_th_ds), 2, lambda_ds, -lambda_gamma))
          # Add terms to the total amplitude multiplied by coupling
          ampl1 += h1*c[ (lambda_ds, lambda_gamma, lambda_dst) ]

          h2 = CastComplex(Wignerd(Acos(cos_th_dst), j_dstst2, lambda_ds, lambda_dst))*\
               Exp(I*CastComplex(lambda_dst/2.*phi_d))*\
               CastComplex(Wignerd(Acos(cos_th_d), 2, lambda_dst, 0))*\
               Exp(I*CastComplex(lambda_ds/2.*phi_ds))*\
               CastComplex(Wignerd(Acos(cos_th_ds), 2, lambda_ds, -lambda_gamma))
          # Add terms to the total amplitude multiplied by coupling
          ampl2 += h2*d[ (lambda_ds, lambda_gamma, lambda_dst) ]
          # Dynamical term (Breit-Wigner) for D1(2420) state (J^P=1+)
      bw1 = BreitWignerLineShape(m2dstpi, Const(2.420), Const(0.0317), mdst, mpi, mdsst, mb, dr, db, 0, 1)
      ampl1 *= bw1

      bw2 = BreitWignerLineShape(m2dstpi, Const(2.465), Const(0.046),  mdst, mpi, mdsst, mb, dr, db, 0, 1)
      ampl2 *= bw2

      density += Density(ampl1 + ampl2)

    # Density is the abs value squared of the amplitude
    return density

  def momenta(x) : 
    """
      Calculate 4-momenta of the final state particles given 6D phase space variables
    """
    dlz = phsp.Data1(x)  # Dalitz plot variables
    ang = phsp.Data2(x)  # Angles
    m2dstpi = dalitzPhsp.M2ab(dlz)
    cos_th_dst = dalitzPhsp.CosHelicityAB(dlz)    # D* helicity angle
    cos_th_d   = angularPhsp.Coordinate(ang, 0)   # D helicity angle
    phi_d      = angularPhsp.Coordinate(ang, 1)   # azimuthal angle of D*->Dpi
    cos_th_ds  = angularPhsp.Coordinate(ang, 2)   # Ds helicity angle
    phi_ds     = angularPhsp.Coordinate(ang, 3)   # azimuthal angle of Ds*->Dsgamma

    th_dst = Acos(cos_th_dst)
    th_d   = Acos(cos_th_d)
    th_ds  = Acos(cos_th_ds)
    mdstpi = Sqrt(m2dstpi)

    zeros = Zeros(m2dstpi)
    ones = Ones(m2dstpi)

    p4_dz, p4_pis   = FourMomentaFromHelicityAngles(Const(mdst)*ones, Const(mdz)*ones, Const(mpi)*ones, th_d, phi_d)
    p4_dst, p4_pi   = FourMomentaFromHelicityAngles(mdstpi, Const(mdst), Const(mpi), th_dst, zeros)
    p4_ds, p4_gamma = FourMomentaFromHelicityAngles(Const(mdsst), Const(mds), zeros, th_ds, phi_ds)

    p_dsst   = TwoBodyMomentum(Const(mb), Const(mdsst), mdstpi)
    p4_dsst  = LorentzVector(Vector(zeros, zeros, -p_dsst), Sqrt(p_dsst**2 + mdsst**2))
    p4_dstpi = LorentzVector(Vector(zeros, zeros,  p_dsst), Sqrt(p_dsst**2 + mdstpi**2))

    p4_dz    = RotateLorentzVector(p4_dz, zeros, -th_dst, zeros)
    p4_pis   = RotateLorentzVector(p4_pis, zeros, -th_dst, zeros)
    p4_dz    = BoostFromRest(p4_dz, p4_dst)
    p4_pis   = BoostFromRest(p4_pis, p4_dst)

    p4_ds    = BoostFromRest(p4_ds,    p4_dsst)
    p4_gamma = BoostFromRest(p4_gamma, p4_dsst)
    p4_dst   = BoostFromRest(p4_dst,   p4_dstpi)
    p4_pi    = BoostFromRest(p4_pi,    p4_dstpi)
    p4_dz    = BoostFromRest(p4_dz,    p4_dstpi)
    p4_pis   = BoostFromRest(p4_pis,   p4_dstpi)

    return (p4_dstpi, p4_dsst, p4_dst, p4_pi, p4_dz, p4_pis, p4_ds, p4_gamma)

  def invmasses(mom) : 
    p4_dstpi, p4_dsst, p4_dst, p4_pi, p4_dz, p4_pis, p4_ds, p4_gamma = mom
    m_dstpi = Mass( p4_dstpi )
    m_dspi  = Mass( p4_ds + p4_pi )
    m_dstds = Mass( p4_dst + p4_ds )
    m_dstdspi = Mass( p4_dst + p4_ds + p4_pi )
    return (m_dstpi, m_dspi, m_dstds, m_dstdspi)

  def helangles(mom) : 
    p4_dstpi, p4_dsst, p4_dst, p4_pi, p4_dz, p4_pis, p4_ds, p4_gamma = mom
    th1_ds, phi1_ds, th1_dst, phi1_dst, th1_d, phi1_d = CalculateHelicityAngles( [ [ [ p4_dz, p4_pis ], p4_pi ], p4_ds ] )
    return (th1_ds, phi1_ds, th1_dst, phi1_dst, th1_d, phi1_d)

  def auxinfo(x) : 
    mom = momenta(x)
    l = []
    for m in mom : 
      l += [ XComponent(m), YComponent(m), ZComponent(m), TimeComponent(m) ]
    l += invmasses(mom)
    l += helangles(mom)
    return l

  # Initialise TF
  init = tf.global_variables_initializer()
  sess = tf.Session()
  sess.run(init)

  # Placeholders for data and normalisation samples
  data_ph = phsp.data_placeholder
  norm_ph = phsp.norm_placeholder

  # TF graphs for decay density as functions of data and normalisation placeholders
  data_model = PartialIntegral(model(data_ph), integ_points)
#  data_model = model(data_ph)
  norm_model = model(norm_ph)

  # Uniform mormalisation sample
  norm_sample = sess.run( phsp.UniformSample(1000000) )

  # Calculate the maximum of PDF for accept-reject method
  majorant = EstimateMaximum(sess, norm_model, norm_ph, norm_sample)*1.5
  print("Maximum = ", majorant)

  # Run toy MC with the model defined above
  data_sample = RunToyMC( sess, norm_model, norm_ph, phsp, 10000, majorant, chunk = 1000000)

  # Calculate D* helicity angle which is a function of Dspi inv. mass
  helangle = np.stack( [ sess.run( dalitzPhsp.CosHelicityAB(phsp.Data1(data_sample)) ) ], axis = 1 )
  aux      = np.stack( sess.run( auxinfo( data_sample ) ), axis = 1 ) 
  # Add D* helcitiy angle to the generated toy MC sample
  array = np.concatenate( [data_sample, helangle, aux], axis = 1)

  # Store the result to ROOT file
  names = [ "m2dstpi", "m2dspi", "costhd", "phid", "costhds", "phids", "costhdst" ]
  for mom in [ "dstpi", "dsst", "dst", "pi", "dz", "pis", "ds", "gamma" ] : 
    names += [ mom + "_x", mom + "_y", mom + "_z", mom + "_t" ]
  names += ["m_dstpi", "m_dspi", "m_dstds", "m_dstdspi"]
  names += ["th_ds", "phi_ds", "th_dst", "phi_dst", "th_d", "phi_d"]
  FillRootFile("toy.root", array, names)

  # Negative log likelihood for the fit
  nll = UnbinnedNLL( data_model, Integral( norm_model ) )

  # expand data sample with integration points in phi_ds
  int_sample   = np.linspace(-math.pi, math.pi, integ_points)
  data_sample2 = np.repeat(data_sample[:,0:-1], int_sample.shape[0], axis = 0)
  int_sample2  = np.reshape(np.tile(int_sample, data_sample.shape[0]), (-1,1))
  data_sample  = np.concatenate( [data_sample2, int_sample2], axis = 1 )

  # Perform Minuit minimisation
  result = RunMinuit(sess, nll, { data_ph : data_sample, norm_ph : norm_sample }, useGradient = True )

  # Write results to text file
  WriteFitResults(result, "result.txt")
