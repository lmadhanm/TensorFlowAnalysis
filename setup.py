import setuptools
import os

here = os.path.abspath(os.path.dirname(__file__))

with open(os.path.join(here, "README.md"), "r") as fh:
    long_description = fh.read()

with open(os.path.join(here, 'requirements.txt'), encoding='utf-8') as requirements_file:
    requirements = requirements_file.read().splitlines()

setuptools.setup(
    name="TensorFlowAnalysis",
    version="0.0.1",
    author="LHCb Collaboration",
    author_email=" lhcb-tensorflow-analysis@cern.ch",
    description="A package containing useful functions for amplitude analysis in HEP using tensorflow",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.cern.ch/poluekt/TensorFlowAnalysis/",
    packages=setuptools.find_packages(),
    install_requires=requirements,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: Apache License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)